﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV6
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();
            notebook.AddNote(new Note("Note1", "Today is Monday!"));
            notebook.AddNote(new Note("Note2", "Shopping list: everything.."));
            notebook.AddNote(new Note("This is title ", "text"));

            Iterator iterator = new Iterator(notebook);
            iterator.Current.Show();
            iterator.Next();

            while (iterator.IsDone != true)
            {
                iterator.Current.Show();
                iterator.Next();
            } 
            Box box = new Box();
            box.AddProduct(new Product("Product1", 9.99));
            box.AddProduct(new Product("Product2", 19.99));
            box.AddProduct(new Product("Product3", 29.99));
            BoxIterator boxIterator = new BoxIterator(box);
            boxIterator.Current.ToString();
            boxIterator.Next();
            while(boxIterator.IsDone != true)
            {
                boxIterator.Current.ToString();
                boxIterator.Next();
            }
            Console.ReadLine();
        }
    }
}
